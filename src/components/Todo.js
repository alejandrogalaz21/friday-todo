import React from 'react'
import Jump from 'react-reveal/Jump'

const TodoStatus = ({ status }) => (<span>{status ? ': )' : ': ('}</span>)

const Todo = ({ _id, desc, status }) => {
  return (
    <Jump>
      <li id={`todo-item-${_id}`}>
        {desc}
        <input type='checkbox' defaultChecked={status} />
        <TodoStatus status={status} />
      </li>
    </Jump>
  )
}

export default Todo