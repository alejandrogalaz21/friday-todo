import React from 'react'
import Todo from './Todo'

const TodosList = props => {
  return (
    <div className='row'>
      <div className='col-lg-6 col-md-6 col-xs-12'>
        <ul>
          {props.todos.map(todo => <Todo key={todo.id} {...todo} />)}
        </ul>
      </div>
    </div>
  )
}


export default TodosList