import React, { Component } from 'react'
import TodoForm from './TodoForm'

class AddTodos extends Component {

  render() {
    return (
      <div className='row'>
        <h1>Agregar Tareas</h1>
        <TodoForm
          value={this.props.value}
          onChange={this.props.onChange}
          onSubmit={this.props.onSubmit} />
      </div>
    )
  }
}

export default AddTodos
