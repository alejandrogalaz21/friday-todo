import React from 'react'

const TodoForm = props => {
  debugger
  return (
    <div className='col-lg-6 col-md-6 col-xs-12'>
      <form onSubmit={props.onSubmit}>
        <input
          id='desc-input'
          type='text'
          name='desc'
          value={props.value}
          onChange={props.onChange} />
        <button type='submit'>Agregar</button>
      </form>
    </div>
  )
}

export default TodoForm
