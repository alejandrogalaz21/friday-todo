import React, { Component } from 'react'
import AddTodos from './components/AddTodos'
import TodosList from './components/TodosList'
import { isEmpty, getRandomInt, newTodo, addItem } from './helpers'
export class App extends Component {

  state = {
    desc: '',
    todos: [
      { _id: 1, desc: 'Lavar la ropa ', status: false },
      { _id: 2, desc: 'Sacar al perro ', status: false },
      { _id: 3, desc: 'Estudiar', status: true }
    ]
  }



  onChange = event => {

    console.info('onChange')
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({ [name]: value })
  }

  onSubmit = event => {

    event.preventDefault()
    const { desc } = this.state
    if (!isEmpty(desc)) {

      const todo = newTodo(getRandomInt(1, 100))(desc)
      const todos = addItem(this.state.todos, todo)
      const state = { desc: '', todos }
      this.setState({ ...state })
      return
    }
    alert('Agregar Tarea')
  }

  render() {
    const { desc } = this.state
    return (
      <div className="container">
        {/* Add todos Section */}
        <AddTodos
          value={desc}
          onChange={this.onChange}
          onSubmit={this.onSubmit} />
        {/* Todos List Section */}
        <TodosList todos={this.state.todos} />
      </div>
    )
  }
}

export default App
