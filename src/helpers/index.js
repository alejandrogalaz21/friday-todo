//* HELPERS FUNCTIONS

/**
 * @param 
 * @return
 * @description
 */
export const formater = list => JSON.stringify(list, null, 2);

/**
 * @param 
 * @return
 * @description Will return a number inside the given range, inclusive of both minimum 
 *              and maximum i.e. if min=0, max=20, returns a number from 0-20.
 */
export const getRandomInt = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

/**
 * @param 
 * @return
 * @description
 */
export const newTodo = randomIntFunction => todo => {
  const _id = randomIntFunction
  const desc = todo
  const status = false
  const result = { _id, desc, status }
  return result
}

/**
 * @param 
 * @return
 * @description
 */
export const isEmpty = value => {
  if ((value === null) || (value === undefined) ||
    (Array.isArray(value) && value.length === 0) ||
    (typeof value === 'string' && value.trim().length === 0) ||
    (typeof value === 'object' && Object.keys(value).length === 0)) {
    return true
  }
  return false
}


//* ( C )( R )( U )( D )

/**
 * @param 
 * @return
 * @description Add a new item to the Array
 */
export const addItem = (list, item) => [...list, item]

/**
 * @param 
 * @return
 * @description Find item by id from a Array
 */
export const findItem = (list, _id) => equal => list.find(equal)

/**
 * @param 
 * @return
 * @description Update item from a Array
 */
export const updatedItem = (list, item) => equal => {
  const index = list.findIndex(equal)
  return [...list.slice(0, index), item, ...list.slice(index + 1)]
}

/**
 * @param 
 * @return
 * @description Delete a item from a Array
 */
export const deleteItem = list => equal => list.filter(equal)

/**
 * @param 
 * @return
 * @description Generates a random number
 */
export const generateId = () => Math.floor(Math.random() * 1000)


